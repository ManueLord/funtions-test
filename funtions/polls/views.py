from django.shortcuts import render, redirect
from django.http import HttpResponse
import cgi

# Create your views here.
def index(request):
    return render(request, "index.html")

def noReturn(request):
    return render(request, "noReturn.html")

def test(request):
    return render(request, "test.html")

cont = 0
def click(request):
    cont = 0
    if request.method == 'POST':
        r1 = int(request.POST.get("cont"))
        cont = r1 +1
    context = {'cont': cont}
    return render(request, "click.html", context)

def selectTime(request):
    return render(request, "selectTime.html")

def timer(request):
    if request.method == 'POST':
        h = request.POST.get("hour")
        m = request.POST.get("min")
        s = "10"
    context = {'hour' : h, 'minute' : m, 'secundo': s}
    return render(request, "timer.html", context)
