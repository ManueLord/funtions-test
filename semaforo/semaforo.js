var minutes = window.prompt('Ingresa el tiempo en minutos');
var time = minutesToMilli(minutes);
var times = timesByColor(time);

highlight('green');
window.setTimeout(function() {
  highlight('ambar');
  window.setTimeout(function() {
    highlight('red');
  }, times.ambar);
}, times.green);


function minutesToMilli() {
  return minutes * 60000;  
}

function timesByColor(t) {
  var time = t;
  var g = time * .66;
  time -= g;
  var a = time / 3;
  time -= a;
  var r = time;
  
  return {
    green: g,
    ambar: a,
    red: r
  }
}

function highlight(color) {
  var actives = document.querySelectorAll('.spotlight.highlight');
  [].forEach.call(actives, function(active) {
    active.classList.remove('highlight');
  });
  var spotlight = document.querySelector(`.spotlight.${color}`);
  spotlight.classList.add('highlight');
}
